;; This "home-environment" file can be passed to 'guix home reconfigure'
;; to reproduce the content of your profile.  This is "symbolic": it only
;; specifies package names.  To reproduce the exact same profile, you also
;; need to capture the channels being used, as returned by "guix describe".
;; See the "Replicating Guix" section in the manual.

(use-modules (gnu home)
             (gnu packages)
             (gnu services)
             (guix gexp)
             (gnu home services desktop)
             (gnu home services shells))

(home-environment
  ;; Below is the list of packages that will show up in your
  ;; Home profile, under ~/.guix-home/profile.
 (packages (specifications->packages (list
                                      "adb"
                                      "ansible"
                                      "freecad"
                                      "cpio"
                                      "docker"
                                      "emacs"
                                      "fastboot"
                                      "file"
                                      "firefox"
                                      "font-gnu-unifont"
                                      "font-google-material-design-icons"
                                      "font-google-noto"
                                      "font-google-noto-emoji"
                                      "font-google-roboto"
                                      "font-hack"
                                      "font-liberation"
                                      "font-microsoft-web-core-fonts"
                                      "font-microsoft-webdings"
                                      "font-openmoji"
                                      "gimp"
                                      "git"
                                      "gnome"
                                      "gnome-backgrounds"
                                      "gnome-bluetooth"
                                      "gnome-shell-extensions"
                                      "gnome-terminal"
                                      "gnome-themes-extra"
                                      "gnome-tweaks"
                                      "gptfdisk"
                                      "gvfs" ;; for user mounts
                                      "icecat"
                                      "icedove"
                                      "keepassxc"
                                      "kicad"
                                      "lsh"
                                      "make"
                                      "network-manager-openconnect"
                                      "network-manager-openvpn"
                                      "nss-certs" ;; for HTTPS access
                                      "ntfs-3g"
                                      "obs"
                                      "openconnect"
                                      "openssh"
                                      "openvpn"
                                      "p7zip"
                                      "pavucontrol"
                                      "pigz"
                                      "pijul"
                                      "pixz"
                                      "python"
                                      "qemu"
                                      "remmina"
                                      "ruby"
                                      ;"shadowsocks"
                                      "screen"
                                      ;"shotcut"
                                      "system-config-printer"
                                      "transmission"
                                      "transmission-remote-gtk"
                                      "ungoogled-chromium"
                                      "unrar-free"
                                      "unzip"
                                      "virt-manager"
                                      "vlc"
                                      "wireguard-tools"
                                      "xlsfonts")))

  ;; Below is the list of Home services.  To search for available
  ;; services, run 'guix home search KEYWORD' in a terminal.
  (services
   (list (service home-bash-service-type
                  (home-bash-configuration
                   (aliases '(("grep" . "grep --color=auto") ("ll" . "ls -l")
                              ("ls" . "ls -p --color=auto")))
                   (bashrc (list (local-file
                                  "/home/camel/git/guixies/home/bashrc"
                                  "bashrc")))
                   (bash-profile (list (local-file
                                        "/home/camel/git/guixies/home/bash_profile"
                                        "bash_profile")))))
         (service home-dbus-service-type (home-dbus-configuration)))))
