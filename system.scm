(use-modules (gnu)
             (gnu system nss)
             (nongnu packages mozilla)
             (nongnu packages fonts)
             (linux-nonfree)
             (reiser4))

(use-service-modules cups desktop virtualization xorg)
(use-package-modules admin
                     base
                     certs
                     cryptsetup
                     disk
                     dns
                     gnome
                     gnuzilla
                     linux
                     networking
                     password-utils
                     ssh
                     video
                     virtualization
                     vpn
                     xorg)

(operating-system
  (host-name "camel")
  (timezone "Europe/Moscow")
  (locale "en_US.utf8")
  (kernel linux-reiser4)
  (initrd-modules (cons* "dm-crypt" %base-initrd-modules))
  ;(initrd-modules (cons* "dm-crypt" "reiser4" %base-initrd-modules))
  (firmware (cons* firmware-nonfree %base-firmware))
  (keyboard-layout
    (keyboard-layout
      "us,ru"
      #:options
      '("grp:caps_toggle")))
  ;; Use the UEFI variant of GRUB with the EFI System
  ;; Partition mounted on /boot/efi.
  (bootloader (bootloader-configuration
                (bootloader grub-efi-bootloader)
                (targets '("/boot/efi"))
                (keyboard-layout keyboard-layout)))

  (mapped-devices
   (list (mapped-device
          (source "vg")
          (targets (list "vg-swap" "vg-guix"))
          (type lvm-device-mapping))))

  (file-systems (append
                 (list (file-system
                         (device "/dev/mapper/vg-guix")
                         (mount-point "/")
                         (type "ext4")
                         (needed-for-boot? #t)
                         (dependencies mapped-devices))
                       (file-system
                         (device (uuid "DC8D-EAAB" 'fat32))
                         (mount-point "/boot")
                         (type "vfat")))
                 %base-file-systems))

  (swap-devices
    (list (uuid "8a902bc7-ad2a-4ce6-bef5-0cd712216209")))

  ;; Create user `camel'.
  (users (cons* (user-account
                  (name "camel")
                  (comment "Camel")
                  (group "users")
                  (home-directory "/home/camel")
                  (supplementary-groups
                    '("wheel" "netdev" "audio" "video" "lp" "libvirt")))
                %base-user-accounts))

  ;; This is where we specify system-wide packages.
  (packages (append (list
                     bridge-utils
                     cryptsetup
                     dnsmasq
                     gnome
                     gvfs
                     lsh
                     lvm2
                     mdadm
                     nss-certs
                     ntfs-3g
                     openconnect
                     openssh
                     openvpn
                     reiser4progs
                     sudo
                     wireguard-tools)
                    %base-packages))
  ;; Add GNOME and Xfce---we can choose at the log-in screen
  ;; by clicking the gear.  Use the "desktop" services, which
  ;; include the X11 log-in service, networking with
  ;; NetworkManager, and more.
  (services (append (list (service gnome-desktop-service-type)
                          (service xfce-desktop-service-type)
                          (service cups-service-type)
                          (service bluetooth-service-type)
                          (service libvirt-service-type
                                   (libvirt-configuration
                                    (unix-sock-group "libvirt")))
                          (service virtlog-service-type
                                   (virtlog-configuration
                                    (max-clients 1000)))
                          (set-xorg-configuration
                           (xorg-configuration
                            (keyboard-layout keyboard-layout))))
                    (modify-services %desktop-services
                                     (guix-service-type
                                      config =>
                                      (guix-configuration
                                       (inherit config)
                                       (substitute-urls '("https://bordeaux.guix.gnu.org"
                                                          "https://substitutes.nonguix.org"))
                                       (authorized-keys (cons*
                                                         (local-file "/etc/guix/nonguix.pub")
                                                         (local-file "/etc/guix/bordeaux.pub")
                                                         %default-authorized-guix-keys)))))))
                                                         ;; (substitute-urls '("https://substitutes.nonguix.org"))
                                                         ;; (authorized-keys (cons*
                                                         ;;                   (local-file "/etc/guix/nonguix.pub")
                                                         ;;                   %default-authorized-guix-keys))
                                                         ;;)))))

  ;; Allow resolution of '.local' host names with mDNS.
  (name-service-switch %mdns-host-lookup-nss))
