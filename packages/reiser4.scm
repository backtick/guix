(define-module (reiser4)
  #:use-module (gnu packages linux)
  #:use-module (guix build-system gnu)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix licenses)
  #:use-module (guix packages)
  #:use-module (ice-9 optargs))

(define-public libaal
  (package
    (name "libaal")
    (version "1.0.7")
    (source (origin
              (method url-fetch)
              (uri
               (string-append "https://sourceforge.net/projects/reiser4/files/reiser4-utils/"
                              name "/" name "-" version ".tar.gz"))
              (sha256
               (base32 "14cqmgdbnw652xh70cxa529axjavvy62fm84d66jcm4gkn36i1bw"))))
    (build-system gnu-build-system)
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (add-after 'unpack 'prepare-build
           (lambda _
             (invoke "ln" "-s" "-f" (which "true") "run-ldconfig"))))))
    (synopsis "Library, that provides application abstraction mechanism.")
    (description
     "This is a library, that provides application abstraction mechanism.
It includes device abstraction, libc independence code, etc.")
    (home-page "https://sourceforge.net/projects/reiser4/files/reiser4-utils/libaal/")
    (license gpl2)))

(define-public reiser4progs
  (package
   (name "reiser4progs")
   (version "1.2.2")
   (source (origin
            (method url-fetch)
            (uri
             (string-append "https://sourceforge.net/projects/reiser4/files/reiser4-utils/"
                            name "/" name "-" version ".tar.gz"))
            (sha256
             (base32 "1gmxll3qm67q4xsbnizgbvnbhacrkivdj21abff8c3n5i0pldpqx"))))
   (build-system gnu-build-system)
   (native-inputs
    (list libaal))
   (inputs
    (list libaal))
   (arguments
    `(#:phases
      (modify-phases %standard-phases
                     (add-after 'unpack 'prepare-build
                                (lambda _
                                  (invoke "ln" "-s" "-f" (which "true") "run-ldconfig"))))))
   (synopsis "Library for Reiser4 filesystem access and manipulation and Reiser4 utilities.")
   (description
    "It contains the library for Reiser4 filesystem access and manipulation and Reiser4 utilities.")
   (home-page "https://sourceforge.net/projects/reiser4/files/reiser4-utils/reiser4progs/")
   (license gpl2)))

(define-public reiser4progs-2
  (package
    (name "reiser4progs-2")
    (version "2.0.5")
    (source (origin
              (method url-fetch)
              (uri
               (string-append "https://sourceforge.net/projects/reiser4/files/v5-unstable/progs/"
                              name "-" version ".tar.gz"))
              (sha256
               (base32 "0b7d7qrwvsr6gqq0cn6jf3bxxd884ffip0vrm0dfxfbsk05pc50c"))))
    (build-system gnu-build-system)
    (native-inputs
     (list libaal `(,util-linux "lib") e2fsprogs))
    (inputs
     (list libaal `(,util-linux "lib") e2fsprogs))
    (arguments
     (list
      #:phases
      #~(modify-phases %standard-phases
          (add-after 'unpack 'prepare-build
            (lambda _
              (invoke "ln" "-s" "-f" (which "true") "run-ldconfig"))))
      #:configure-flags
      #~(list
         (string-append "--with-libaal=" #$(this-package-input "libaal"))
         "--with-uuid")))
    (synopsis "Library for Reiser5 filesystem access and manipulation and Reiser5 utilities.")
    (description
     "It contains the library for Reiser5 filesystem access and manipulation and Reiser5 utilities.")
    (home-page "https://sourceforge.net/projects/reiser4/files/reiser4-utils/reiser4progs/")
    (license gpl2)))
